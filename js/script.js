var myapp = angular.module('appModule', ['restangular', 'ui.router'])
    .config(function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("/home");
        $stateProvider
            .state("studentParent",{
                url:"/home",
                templateUrl:"Templates/studentParent.html",
                controller:"stdParentController",
                abstract:true
            })
            .state("studentParent.home", {
                url: "/list",
                templateUrl: "Templates/home.html",
                controller: "displayController"
            })
            .state("studentParent.studentDetails", {
                url: "/details/:studentId",
                templateUrl: "Templates/studentDetails.html",
                controller: "detailsController"
            })
            .state("add", {
                url: "/add",
                templateUrl: "Templates/add.html",
                controller: "addController"
            })
            .state("update", {
                url: "/update/:studentId",
                templateUrl: "Templates/update.html",
                controller: "updateController"
            })
    })
    .controller('stdParentController', ['RestMethodsService', '$scope', function(RestMethodsService, $scope){
        RestMethodsService.getTotal().then(function(data){
            $scope.studentTotal = data;
        })
    }])
    .controller('displayController', ['RestMethodsService', '$scope', '$state', function(RestMethodsService, $scope, $state) {
        RestMethodsService.getUsers().then(function(data) {
            $scope.dataset = data;
            console.log(data);
        }, function() {
            console.log("error");
        });
    }])
    .controller('detailsController', ['RestMethodsService', '$stateParams', '$scope','$state', function(RestMethodsService, $stateParams, $scope, $state) {
        var studentId = $stateParams.studentId;
        RestMethodsService.findOneStudent(studentId).then(function(response) {
            $scope.studentDetails = response;
        });
        $scope.deleteRecord = function(student) {
            RestMethodsService.terminateStudent(student.studentId).then(function() {
                console.log("Terminated Successfully!")
                $state.go("studentParent.studentDetails", {}, {
                    reload: true
                });
            }, function() {
                console.log("error");
            })
        }
    }])
    .controller('addController', ['RestMethodsService', '$scope', function(RestMethodsService, $scope) {
        $scope.add = function(student) {
            RestMethodsService.createStudent(student).then(function(data) {
                console.log("form Submitted Successfully" + data);
            }, function() {
                console.log("error in form submission");
            });
        }
    }])
    .controller('updateController', ['RestMethodsService', '$scope', '$stateParams', '$state', function(RestMethodsService, $scope, $stateParams, $state) {
        var studentId = $stateParams.studentId;
        console.log(studentId);
        $scope.student = {};
        $scope.oldStudent = RestMethodsService.findOneStudent(studentId).then(function(response) {
            $scope.student = response;
        });

        $scope.update = function(student) {
            RestMethodsService.updateStudent(studentId, student).then(function() {
                console.log("data updated successfully!!")
                $state.go("studentParent.home");
            }, function() {
                console.log("error in data update")
            });
        }
    }])